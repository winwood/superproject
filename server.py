#!/usr/bin/env python3
'''
    author winw00d
    license ecl-2.0
    udp server with RSA
    Project "Primitive RSA for education"
'''
import argparse
import socket
import json
import datetime
import rsa_cipher
import backend

parser = argparse.ArgumentParser(description='Client for secret service', add_help=True)
parser.add_argument('-a','--address',
                    default='0',
                    help="server address (domain or IP)")
parser.add_argument('-k','--open-key',
                    dest='keyFile',
                    required=True,
                    type=argparse.FileType('r', encoding='UTF-8'),
                    help="read client RSA open key from FILE",
                    metavar='FILE')
parser.add_argument('-p','--port',
                    dest='port',
                    help="server port number (default: %(default)s) ",
                    type=int,
                    default=7777)
args = parser.parse_args()
    

rsa = rsa_cipher.Rsa(json.load(args.keyFile))

addr = (args.address,args.port)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(addr)

try:
    while True:
        data, addr = sock.recvfrom(1024)
        message = ''
        cmd = None
        date = None
        for m in data.decode().split('\n'):
            if m.split(':')[0] == 'Signature':
                sig = m.split(':')[1].lstrip()
                break
            else:
                message = message + m +'\n'
                if m.split(':')[0] == 'Command':
                    cmd = m[8:].lstrip()
                elif m.split(':')[0] == 'Date':
                    date = datetime.datetime.fromisoformat(m[5:].lstrip())
        else:
            sock.sendto('Error: no signature\n'.encode(), addr)
            continue
        if not rsa.checkSig(message,sig):
            sock.sendto('Error: wrong signature\n'.encode(), addr)
            continue
        if date is None:
            sock.sendto('Error: no date\n'.encode(), addr)
            continue
        if (datetime.datetime.today() - date).total_seconds() > 60:
            sock.sendto('Error: message is too old\n'.encode(), addr)
            continue      
        if cmd is None:
            sock.sendto('Error: no command\n'.encode(), addr)
            continue
        isEncr, answer = backend.processing(cmd)
        if isEncr:
            answer = 'Answer: ' + rsa.encrypt(answer)
        else:
            answer = 'Comment: ' + answer
        answer = 'Command: '+ cmd + '\n' + answer + '\n'
        answer = answer + 'Origin: https://gitlab.com/winwood/superproject\n'
        sock.sendto(answer.encode(), addr)
        
except KeyboardInterrupt:
    sock.close()
