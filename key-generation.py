#!/usr/bin/env python3
'''
    author winw00d
    license ecl-2.0
    rsa key generator
    Project "Primitive RSA for education"
'''
import math
import primes
import json
import argparse

three_bit_primes = [3618502788666131106986593281521497120414687020801267626233049500247285301313,
                    1645504557321206042154969182557350504982735865633579863348609057,
                    102844034832575377634685573909834406561420991602098741459288097]

#get command arguments
def get_args():
    parser = argparse.ArgumentParser(description='RSA key generator',
                                     add_help=True)
    parser.add_argument("keyIndex",
                        type=int,
                        choices=range(len(three_bit_primes)),
                        help='index of base open key exponent'),
    parser.add_argument('-o','--open-key',
                        dest='openKeyFile',
                        type=argparse.FileType('w',  encoding='UTF-8'),
                        default='-',
                        help="write RSA open key to FILE",
                        metavar='FILE')
    parser.add_argument('-p', '--key-pair',
                        dest='pairKeyFile',
                        type=argparse.FileType('w',  encoding='UTF-8'),
                        default='-',
                        help="write RSA key pair to FILE",
                        metavar='FILE')
    return parser.parse_args()

args = get_args()

#base prime numbers
q = primes.gen_prime(120)
p = primes.gen_prime(129)

#modulus
n = p * q

print("q =", q)
print("p =", p)
print("n =", n)

#Carmichael's totient function 
ln = (p - 1) // math.gcd(p - 1, q - 1) * (q - 1)
print("λ(n) =", ln)

#open key - prime number
e = three_bit_primes[args.keyIndex]
open_key = [ e, n ]

#secret key - coprime number
d = primes.inverse(e, ln)
secret_key =  [ d, n ]

#output key pair as json
rsa_pair = {"RSA key pair" : {"Open key" : open_key, "Secret key" :secret_key }}
json.dump(rsa_pair,args.pairKeyFile)
if args.pairKeyFile.isatty():
    print(file=args.pairKeyFile)
else:
    args.pairKeyFile.close()

#output open key as json
rsa_pair['RSA key pair']['Secret key'] = None
json.dump(rsa_pair,args.openKeyFile)
if args.openKeyFile.isatty():
    print(file=args.openKeyFile)
else:
    args.openKeyFile.close()


