Superproject
------------

Реализация алгоритма RSA с коротким ключом на примере клиент-серверного приложения обмена данными

Предназначен для изучения атаки факторизации открытого ключа с помощью таких пакетов, как cado_nfs или msieve

При создании файла примера intercept.pcap использовался один из открытых ключей, находящихся в репозитории.