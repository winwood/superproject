#!/usr/bin/env python3
'''
    author winw00d
    license ecl-2.0
    rsa lib for cryptography and digital signatures
    Project "Primitive RSA for education"
'''
import hashlib

class RsaException(Exception):
    pass


class Rsa(object):
    def __init__(self, key_pair):
        self.open_key = key_pair['RSA key pair']['Open key']
        self.secret_key = key_pair['RSA key pair']['Secret key']

    def encrypt(self, message):
        num_mess = int.from_bytes(message.encode(),'little')
        if num_mess > self.open_key[1]:
            raise RsaException('Encrypt error: Message too long')
        enc_m = hex(pow(num_mess, self.open_key[0], self.open_key[1]))
        return enc_m[2:]
        
    def decrypt(self, cryptomess):
        if self.secret_key is None:
            raise RsaException('Decrypt error: No secret key')
        decr_m = pow(int(cryptomess,16), self.secret_key[0], self.secret_key[1])
        mess = decr_m.to_bytes(decr_m.bit_length()//8+1,'little')
        return mess.decode()

    def makeSig(self, message):
        if self.secret_key is None:
            raise RsaException('makeDigest error: No secret key')
        dg = int.from_bytes(hashlib.md5(message.encode()).digest(),'little')
        encr_dg = hex(pow(dg, self.secret_key[0], self.secret_key[1]))
        return encr_dg[2:]
    
    def checkSig(self, message, sig):
        dg = int.from_bytes(hashlib.md5(message.encode()).digest(),'little')
        decr_dg = pow(int(sig,16), self.open_key[0], self.open_key[1])
        return dg == decr_dg
'''
key={"RSA key pair": {"Open key": [3618502788666131106986593281521497120414687020801267626233049500247285301313, 25921978276074270525240287759060903631861239867052888299278709942952938250709], "Secret key": [1690331895824919302031008523376808184245119828436644318917242423085624056277, 25921978276074270525240287759060903631861239867052888299278709942952938250709]}}
message = "Ave_M{not_use_too_short_keys}"
rsa=Rsa(key)
try:
    cryp = rsa.encrypt(message)
    print(cryp)
    print(rsa.decrypt(cryp))
    dig_sig = rsa.makeSig(message)
    print(dig_sig)
    print(rsa.checkSig(message,dig_sig))
except Exception as e:
    print(e)
'''